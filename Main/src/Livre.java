public class Livre {
    private int numLivre;
    private String titre;
    private int nombreDePages;
    private Personne auteur;
    private static int dernierNum = 0;

    public Livre(String titre, int nombreDePages, Personne auteur) {
        this.titre = titre;
        this.nombreDePages = nombreDePages;
        this.auteur = auteur;

        dernierNum++;
        this.numLivre = dernierNum;
    }



    public int getNumLivre() {
        return numLivre;
    }

    public void setNumLivre(int numLivre) {
        this.numLivre = numLivre;
    }


    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


    public int getNombreDePages() {
        return nombreDePages;
    }

    public void setNombreDePages(int nombreDePages) {
        this.nombreDePages = nombreDePages;
    }


    public Personne getAuteur() {
        return auteur;
    }

    public void setAuteur(Personne auteur) {
        this.auteur = auteur;
    }


    public int getDernierNum() {
        return dernierNum;
    }

    public void setDernierNum(int dernierNum) {
        this.dernierNum = dernierNum;
    }


    @Override
    public String toString() {
        return "numLivre=" + numLivre +
                ", titre='" + titre + '\'' +
                ", nombreDePages=" + nombreDePages +
                ", auteur=" + auteur +
                ", dernierNum=" + dernierNum ;
    }
}
