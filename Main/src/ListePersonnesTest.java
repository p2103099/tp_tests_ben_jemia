import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListePersonnesTest {

    private Personne पास्कल;

    private ListePersonnes conteneur = new ListePersonnes();

    public ListePersonnesTest() {
    }
    @BeforeClass
    public static void setUpClass() {
    }
    @AfterClass
    public static void tearDownClass() {
    }
    @Before
    public void setUp() {
        पास्कल = new Personne("बर्थियर","पास्कल",1967);
        //Ça provoquait une erreur en instanciant le conteneur de gens ici
    }
    @After
    public void tearDown() {
    }

    @Test
    void ajouterPersonne() {
        conteneur.ajouterPersonne(पास्कल);
        assertEquals(1,conteneur.getConteneurDeGens().size());
        assertEquals(पास्कल,conteneur.getConteneurDeGens().get(0));
    }

    @Test
    void containsPersonne() {
        conteneur.ajouterPersonne(पास्कल);
        assertEquals(true,conteneur.containsPersonne(पास्कल));
    }
}