import java.util.ArrayList;

public class ListePersonnes {
    private static ArrayList<Personne> conteneurDeGens;

    public ListePersonnes() {
        conteneurDeGens = new ArrayList<Personne>();
    }


    public void ajouterPersonne(Personne p) {
        conteneurDeGens.add(p);
    }

    public boolean containsPersonne(Personne p) {
        return conteneurDeGens.contains(p);
    }


    public ArrayList<Personne> getConteneurDeGens() {
        return conteneurDeGens;
    }
}
