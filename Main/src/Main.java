import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bibliotheque bibliotheque = new Bibliotheque();


        Scanner ask = new Scanner(System.in);

        int choix = 0;

        System.out.println("1- Ajouter un livre" +
                "2- Afficher les livres de la bibliothèque" +
                "3- Rechercher un livre par son titre" +
                "4- Rechercher un livre par son auteur" +
                "N'importe quoi d'autre: quittez le programme");

        String objet;

        Livre livre = null;

        while(true) {
            choix = ask.nextInt();
            switch (choix) {
                case 1:
                    System.out.println("Titre?");
                    livre.setTitre(ask.nextLine());
                    System.out.println("Auteur? (Nom, Prénom, Année de Naissance)");
                    livre.setAuteur(new Personne(ask.nextLine(),ask.nextLine(),ask.nextInt()));
                    System.out.println("Nombre pages?");
                    livre.setNombreDePages(ask.nextInt());
                    bibliotheque.ajouterLivre(livre);
                    break;
                case 2:
                    bibliotheque.afficherBibliotheque();
                    break;
                case 3:
                    System.out.println("Titre Livre?");
                    objet = ask.nextLine();
                    bibliotheque.afficherLivreTitre(objet);
                    break;
                case 4:
                    System.out.println("Nom/Prénom Auteur?");
                    objet = ask.nextLine();
                    bibliotheque.afficherLivreAuteur(objet);
                    break;
                default:
                    System.exit(1);
            }
        }
    }
}
