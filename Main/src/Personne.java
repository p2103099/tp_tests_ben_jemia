public class Personne {
    private int numeroPers;
    private String nomPers;
    private String prenomPers;
    private int anNaissance;
    private static int dernierNumero = 0;


    public Personne(String nom, String prenom, int anNaissance) {
        this.nomPers = nom;
        this.prenomPers = prenom;
        this.anNaissance = anNaissance;

        dernierNumero++;
        this.numeroPers = dernierNumero;
    }

    public int getNumeroPers() {
        return this.numeroPers;
    }


    public String getNomPers() {
        return this.nomPers;
    }

    public String getPrenomPers() {
        return this.prenomPers;
    }

    public int getAnNaissance() {
        return this.anNaissance;
    }

    public static int getDernierNumero() {
        return dernierNumero;
    }


    public void setNumeroPers(int numeroPers) {
        this.numeroPers = numeroPers;
    }

    public void setPrenomPers(String prenomPers) {
        this.prenomPers = prenomPers;
    }

    public void setNomPers(String nomPers) {
        this.nomPers = nomPers;
    }

    public void setAnNaissance(int anNaissance) {
        this.anNaissance = anNaissance;
    }


    public String toString() {
        return this.nomPers+", "+this.prenomPers+", "+this.anNaissance+", n°"+this.numeroPers;
    }
}
