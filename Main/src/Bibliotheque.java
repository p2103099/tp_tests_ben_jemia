import java.util.ArrayList;
import java.util.Scanner;

public class Bibliotheque {
    private ListePersonnes conteneur;
    private Personne alan;
    private Livre computing;
    private ArrayList<Livre> catalogue = new ArrayList<Livre>();

    public Bibliotheque() {
        this.conteneur  = new ListePersonnes();
        this.alan = new Personne("Turing", "Alan", 1912);
        this.computing = new Livre("Computing Machinery and Intelligence", 250,alan);
        this.conteneur.ajouterPersonne(alan);
        this.catalogue.add(computing);
    }

    public void ajouterLivre(Livre livre) {
        this.catalogue.add(livre);
    }

    public void afficherBibliotheque() {
        System.console().writer().println(this.catalogue);
    }

    public void afficherLivreTitre(String titre) {
        for (Livre livre: this.catalogue) {
            if (livre.getTitre().contains(titre)) {
                System.out.println(livre);
            }
        }
    }

    public void afficherLivreAuteur(String auteur) {
        for (Livre livre: this.catalogue) {
            if (livre.getAuteur().getNomPers().contains(auteur)||livre.getAuteur().getPrenomPers().contains(auteur)) {
                System.out.println(livre);
            }
        }
    }

    public ArrayList<Livre> getCatalogue() {
        return this.catalogue;
    }


}
