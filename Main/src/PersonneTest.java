import org.junit.*;

import static org.junit.jupiter.api.Assertions.*;

class PersonneTest {

    private Personne alan = new Personne("Turing", "Alan", 1912);
    public PersonneTest() {
    }
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }
    @Before
    public void setUp() {
        //Ça provoquait une erreur en instanciant Alan ici
    }
    @After
    public void tearDown() {
    }


    @org.junit.jupiter.api.Test
    void testGetNumeroPers() {
        Personne daniel = new Personne("Eriksen", "Daniel", 2040);
        assertEquals(1,alan.getNumeroPers());
        assertEquals(2,daniel.getNumeroPers());
    }

    @org.junit.jupiter.api.Test
    void testGetNomPers() {
        assertEquals("Turing", alan.getNomPers());
    }

    @org.junit.jupiter.api.Test
    void testGetPrenomPers() {
        assertEquals("Alan", alan.getPrenomPers());
    }

    @org.junit.jupiter.api.Test
    void testGetAnNaissance() {
        assertEquals(1912, alan.getAnNaissance());
    }

    @org.junit.jupiter.api.Test
    void testGetDernierNumero() {
        Personne daniel = new Personne("Eriksen", "Daniel", 2040);
        assertEquals(2, Personne.getDernierNumero());
    }

    @org.junit.jupiter.api.Test
    void testSetNumeroPers() {
        alan.setNumeroPers(18);
        assertEquals(18, alan.getNumeroPers());
    }

    @org.junit.jupiter.api.Test
    void testSetNomPers() {
        alan.setNomPers("Minsky");
        assertEquals("Minsky", alan.getNomPers());
    }
    @org.junit.jupiter.api.Test
    void testSetPrenomPers() {
        alan.setPrenomPers("Marvin");
        assertEquals("Marvin", alan.getPrenomPers());
    }



    @org.junit.jupiter.api.Test
    void testSetAnNaissance() {
        alan.setAnNaissance(1990);
        assertEquals(1990, alan.getAnNaissance());
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        assertEquals("Turing, Alan, 1912, n°1", alan.toString());
    }
}