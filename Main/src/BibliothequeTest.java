import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BibliothequeTest {

    private Bibliotheque bible = new Bibliotheque();
    private Livre livre_ajout;

    @Before
    public void setUp() {
        livre_ajout = new Livre("caca",22,null);
        //Instancier bible ici cause des erreurs
    }

    @Test
    void ajouterLivre() {
        bible.ajouterLivre(livre_ajout);
        bible.afficherBibliotheque();
        assertEquals(true, System.console().writer().toString().contains(livre_ajout.toString()));
                        //J'ai essayé ici de détecter ce que la console aa affiché et de savoir si dedans il ya
                        //  le toString de livre_ajout
    }

    @Test
    void afficherBibliotheque() {
        StringBuffer get_tostrings = new StringBuffer();
        for (Livre livre: bible.getCatalogue()) {
            get_tostrings.append(livre.toString());
        }
        bible.afficherBibliotheque();
        assertEquals(get_tostrings, System.console().writer().toString());
    }

    @Test
    void afficherLivreTitre() {
    }

    @Test
    void afficherLivreAuteur() {
    }
}